package diningphilosophers;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChopStick {

    private static int stickCount = 0;

    private boolean iAmFree = true;
    private final int myNumber;
    private final Lock verrou = new ReentrantLock();

    public ChopStick() {
        myNumber = ++stickCount;
    }

    
    public boolean take() {
        if (verrou.tryLock()) { // on essaie de verrouiller la baguette
            iAmFree = false;
            System.out.println("Stick " + myNumber + " Taken");
            return true; // la baguette est disponible et a été prise par le philosophe.
        }
        return false; // la baguette n'est plus libre
    }

    public void release() {
        verrou.unlock();
        iAmFree = true;
        System.out.println("Stick " + myNumber + " Released");
    }
    

    @Override
    public String toString() {
        return "Stick#" + myNumber;
    }
}
